//
//  JsonExt.h
//  JsonExt
//
//  Created by Dan Kalinin on 11/25/20.
//

#import <JsonExt/JseMain.h>
#import <JsonExt/JseInit.h>

FOUNDATION_EXPORT double JsonExtVersionNumber;
FOUNDATION_EXPORT const unsigned char JsonExtVersionString[];
